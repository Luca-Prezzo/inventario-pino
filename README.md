# Inventario 

## Opzionale:

**Creiamo l'ambiente virtuale:**

```python
python -m venv venv
```

**Su windows:**
```python
venv/Script/activate.bat
```

**Su linux:**
```python
# with bash
source venv/bin/activate

# with fish
source venv/bin/activate.fish 
```


## Prerequisiti
```python
python -m pip install -r requirements.txt
```

## Avvio
```python
python main.py
```