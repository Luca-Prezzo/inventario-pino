import os
import shutil

import utils.utils


def init_backup(max_num_backup):
    # se nella cartella ci sono più backup dello stabilito elimino il più vecchio
    if len(os.listdir("./data/backup/")) >= max_num_backup:
        remove_oldest_backup()

    # faccio il backup
    mkbackup()


# rimuovo il backup più vecchio
def remove_oldest_backup():
    backup_list = sort_backup_by_date()
    os.remove(f"./data/backup/{backup_list[0]}")
    utils.utils.log(tipo='info', args='il backup più vecchio è stato eliminato')


# faccio il backup
def mkbackup() -> None:
    current_datetime = utils.utils.get_current_datetime()
    shutil.copy("./data/database.db", f"./data/backup/database_{current_datetime}.db")
    utils.utils.log(tipo='info', args='nuovo backup creato correttamente')


# ordino la lista dei backup in base all'ora di creazione
def sort_backup_by_date() -> list:
    backup_list = os.listdir("./data/backup/")
    backup_list.sort(key=lambda x: os.path.getmtime(backup_list + x))
    return backup_list
