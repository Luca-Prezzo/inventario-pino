import sqlite3


# lancio la query
def run_query(query: str) -> list:
    connection = sqlite3.connect("./data/database.db")

    cursor = connection.cursor()
    cursor.execute(query)
    res = cursor.fetchone()

    connection.commit()
    connection.close()

    return res


# inserisco nuovi prodotti nel database
def query_insert_in_database(codice_prodotto: str, nome_prodotto: str, quantita: int, prezzo_costo: float,
                             prezzo_unitario: float):
    query_inserimento = f"INSERT INTO inventario(codice_prodotto, nome_prodotto, quantita, prezzo_costo, prezzo) " \
                        f"VALUES('{codice_prodotto}', '{nome_prodotto}', {quantita}, {prezzo_costo}, {prezzo_unitario})"
    run_query(query_inserimento)


# rimuovo un prodotto dal database
def query_remove_from_database(codice_prodotto: str):
    query_rimozione = f"DELETE FROM inventario WHERE codice_prodotto='{codice_prodotto}'"
    run_query(query_rimozione)


# aggiorno i prodotti in entrata o uscita
def query_movimento_prodotti(tipo_movimento: str, codice_prodotto: str, quantita: int):
    query_movimento = f"UPDATE inventario SET quantita = quantita{tipo_movimento}{quantita} WHERE codice_prodotto='{codice_prodotto}'"
    run_query(query_movimento)


# ottengo le informazioni sul prodotto
def query_info_prodotto(codice_prodotto: str):
    query_info = f"SELECT * FROM inventario WHERE codice_prodotto='{codice_prodotto}'"
    return run_query(query_info)
