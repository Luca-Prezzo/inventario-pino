from datetime import datetime


# ottengo data e ora
def get_current_datetime() -> str:
    raw_datetime = datetime.now()
    return raw_datetime.strftime("%d_%m_%Y_%H_%M_%S")


# faccio il log
def log(tipo: str, args: str) -> None:
    if tipo == 'corretto':
        msg_type = "NUOVO MOVIMENTO"

    elif tipo == "errore":
        msg_type = 'ERRORE'

    elif tipo == "info":
        msg_type = 'INFO'

    elif tipo == "critico":
        msg_type = "!CRITICO!"

    else:
        msg_type = 'EXTRA'

    log_text = f"[ {get_current_datetime()} ] +- [ {msg_type} ] +- {args}"
    print(log_text)
    with open("./data/log.txt", "a") as data:
        data.write(log_text + "\n")
