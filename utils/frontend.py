import json
import sqlite3
from os import path

import PySimpleGUI as psg
from screeninfo import get_monitors

import utils.backend
import utils.setup


def get_font_size() -> int:
    font_settings = get_settings()
    return font_settings['font']


def get_button_size() -> tuple:
    settings = get_settings()
    return settings['button_width'], settings['button_height']


def get_pad() -> int:
    settings = get_settings()
    return settings['pad']


def get_settings():
    utils.setup.init_setup()

    # se non esiste il file lo creo
    if not path.exists("./data/settings.json"):
        utils.setup.setup_settings_file()

    # prendo il dizionario delle impostazioni
    with open("./data/settings.json", "r") as data:
        return json.load(data)


# prendo la risoluzione del monitor primario
def get_primary_resolution():
    # ottengo la lista dei monitor collegati
    monitor_list = get_monitors()

    # dalla lista dei monitor ritorno la
    # risoluzione del monitor primario
    for monitor in monitor_list:
        if monitor.is_primary:
            return monitor.width, monitor.height


# HARDCODED INFO
WIDTH, HEIGHT = get_primary_resolution()
FONT = ("", get_font_size(), "bold")
SIZE = get_button_size()
PAD = get_pad()


# main menu
def main_menu():
    layout = [
        [psg.Button("PRODOTTO IN ENTRATA", font=FONT, size=SIZE, pad=PAD),
         psg.Button("PRODOTTO IN USCITA", font=FONT, size=SIZE, pad=PAD)],
        [psg.Button("AGGIUNGI PRODOTTO", font=FONT, size=SIZE, pad=PAD),
         psg.Button("INFORMAZIONI PRODOTTO", font=FONT, size=SIZE, pad=PAD)],
        [psg.Button("RIMUOVI PRODOTTO", font=FONT, size=SIZE, pad=PAD),
         psg.Button("ESCI", font=FONT, size=SIZE, pad=PAD)]
    ]

    window = psg.Window(title="Menu", layout=layout, location=(0, 0))

    utils.utils.log(tipo='info', args='lancio il menù')

    while True:

        event, values = window.read()

        if event in (psg.WIN_CLOSED, 'ESCI'):
            with open("./data/log.txt", "a") as data:
                data.write("-" * 125 + "\n")
            break

        elif event == 'PRODOTTO IN ENTRATA':
            window_movimento_prodotto(tipo_movimento='+')

        elif event == 'PRODOTTO IN USCITA':
            window_movimento_prodotto(tipo_movimento='-')

        elif event == 'AGGIUNGI PRODOTTO':
            window_aggiunta_prodotti()

        elif event == 'RIMUOVI PRODOTTO':
            window_rimuovi_prodotto()

        elif event == 'INFORMAZIONI PRODOTTO':
            window_info_prodotto()


def window_movimento_prodotto(tipo_movimento: str):
    layout = [
        [psg.Text("Codice prodotto", font=FONT), psg.InputText(key='codice', font=FONT, pad=PAD)],
        [psg.Text("Quantità", font=FONT), psg.InputText(key='quantita', font=FONT, pad=PAD)],
        [psg.Button("ESCI", font=FONT, pad=PAD), psg.Button("APPLICA", font=FONT, pad=PAD)],
    ]
    window = psg.Window(title="movimento prodotto", layout=layout, keep_on_top=True)

    while True:

        event, values = window.read()

        if event in (psg.WIN_CLOSED, 'ESCI'):
            window.close()
            break

        elif event == 'APPLICA':

            if any(values[entry] == '' for entry in values):
                psg.popup("Compilare tutti i campi", font=FONT, keep_on_top=True)
                utils.utils.log(tipo='errore', args='campilare tutti i campi')
                continue

            try:
                utils.backend.query_movimento_prodotti(tipo_movimento=tipo_movimento, codice_prodotto=values['codice'],
                                                       quantita=values['quantita'])
                tipologia = 'entrata' if tipo_movimento == '+' else 'uscita'
                utils.utils.log(tipo='corretto',
                                args=f'codice prodotto: {values["codice"]} x{values["quantita"]} in {tipologia}')

                if tipologia == 'uscita':
                    prezzo = utils.backend.query_info_prodotto(values['codice'])[4]
                    prezzo_totale = round(float(prezzo) * float(values['quantita']), 2)
                    prezzo_costo = utils.backend.query_info_prodotto(codice_prodotto=values['codice'])[3]
                    psg.popup(
                        f"PREZZO DI COSTO UNITARIO: {prezzo_costo}€\nPREZZO DI COSTO TOTALE: {prezzo_costo * float(values['quantita'])}€\nPREZZO TOTALE: {prezzo_totale}€",
                        font=FONT, keep_on_top=True)

                else:
                    psg.popup("Articolo aggiornato", font=FONT, keep_on_top=True)

                window.close()


            except (sqlite3.IntegrityError, sqlite3.OperationalError) as e:
                psg.popup("Controllare che i dati siano corretti", font=FONT, keep_on_top=True)
                utils.utils.log(tipo='errore', args=f'{values} impossibile aggiungere al database ({e})')


def window_aggiunta_prodotti():
    layout = [
        [psg.Text("Codice prodotto", font=FONT), psg.InputText(key='codice', font=FONT, pad=PAD)],
        [psg.Text("Nome prodotto", font=FONT), psg.InputText(key='nome', font=FONT, pad=PAD)],
        [psg.Text("Quantità", font=FONT), psg.InputText(key='quantita', font=FONT, pad=PAD)],
        [psg.Text("Prezzo di costo", font=FONT), psg.InputText(key='prezzo_costo', font=FONT, pad=PAD)],
        [psg.Text("Prezzo unitario", font=FONT), psg.InputText(key='prezzo', font=FONT, pad=PAD)],
        [psg.Button("ESCI", font=FONT, pad=PAD), psg.Button("APPLICA", font=FONT, pad=PAD)],
    ]

    window = psg.Window(title='Aggiunta nuovo prodotto', layout=layout, keep_on_top=True)

    while True:
        event, values = window.read()

        if event in (psg.WIN_CLOSED, 'ESCI'):
            window.close()
            break

        if any(values[entry] == '' for entry in values):
            psg.popup("Compilare tutti i campi", font=FONT, keep_on_top=True)
            utils.utils.log(tipo='errore', args='campilare tutti i campi')
            continue

        elif event == 'APPLICA':

            try:
                utils.backend.query_insert_in_database(codice_prodotto=values['codice'], nome_prodotto=values['nome'],
                                                       quantita=values['quantita'], prezzo_costo=values['prezzo_costo'],
                                                       prezzo_unitario=values['prezzo'])
                utils.utils.log(tipo='corretto', args=f'prodotto {values} inserito correttamente')
                psg.popup("Prodotto inserito correttamente", font=FONT, keep_on_top=True)
                window.close()

            except (sqlite3.IntegrityError, sqlite3.OperationalError) as e:
                psg.popup("Controllare che i dati siano corretti", font=FONT, keep_on_top=True)
                utils.utils.log(tipo='errore', args=f'{values} impossibile aggiungere al database ({e})')


def window_rimuovi_prodotto():
    layout = [
        [psg.Text("Codice prodotto", font=FONT), psg.InputText(key='codice', font=FONT, pad=PAD)],
        [psg.Button("ESCI", font=FONT, pad=PAD), psg.Button("APPLICA", font=FONT, pad=PAD)],
    ]

    window = psg.Window(title='Rimuovi prodotto', layout=layout, keep_on_top=True)

    while True:
        event, values = window.read()

        if event in (psg.WIN_CLOSED, 'ESCI'):
            window.close()
            break

        if any(values[entry] == '' for entry in values):
            psg.popup("Compilare tutti i campi", font=FONT, keep_on_top=True)
            utils.utils.log(tipo='errore', args='campilare tutti i campi')
            continue

        elif event == 'APPLICA':

            try:
                utils.backend.query_remove_from_database(codice_prodotto=values['codice'])
                utils.utils.log(tipo='corretto', args=f'prodotto {values} rimosso correttamente')
                psg.popup("Articolo rimosso correttamente", font=FONT, keep_on_top=True)
                window.close()

            except (sqlite3.IntegrityError, sqlite3.OperationalError) as e:
                psg.popup("Controllare che i dati siano corretti", font=FONT, keep_on_top=True)
                utils.utils.log(tipo='errore', args=f'{values} impossibile rimuovere dal database ({e})')


def window_info_prodotto():
    layout = [
        [psg.Text("Codice prodotto", font=FONT), psg.InputText(key='codice', font=FONT, pad=PAD)],
        [psg.Text("", key='info', font=FONT, pad=PAD)],
        [psg.Button("ESCI", font=FONT, pad=PAD), psg.Button("APPLICA", font=FONT, pad=PAD)],
    ]

    window = psg.Window(title='INFO prodotto', layout=layout, keep_on_top=True)

    while True:
        event, values = window.read()

        if event in (psg.WIN_CLOSED, 'ESCI'):
            window.close()
            break

        if any(values[entry] == '' for entry in values):
            psg.popup("Compilare tutti i campi", font=FONT, keep_on_top=True)
            utils.utils.log(tipo='errore', args='campilare tutti i campi')
            continue

        elif event == 'APPLICA':

            try:
                info: list = utils.backend.query_info_prodotto(codice_prodotto=values['codice'])
                text_info = f"CODICE: {info[0]}\nNOME: {info[1]}\nQUANTITÀ: {info[2]}\nPREZZO DI COSTO: {info[3]}€\nPREZZO UNITARIO: {info[4]}€"
                window['info'].update(text_info)
                utils.utils.log(tipo='corretto', args=f'info {info} ottenute correttamente')


            except (sqlite3.IntegrityError, sqlite3.OperationalError, IndexError, TypeError) as e:
                psg.popup("Controllare che i dati siano corretti", font=FONT, keep_on_top=True)
                utils.utils.log(tipo='errore', args=f'{values} impossibile ottenere '
                                                    f'le informazioni dal database ({e})')
