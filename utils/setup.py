import os

from utils.backend import run_query
from utils.utils import log


# faccio il setup del database
def init_setup():
    # se non esiste la cartella con tutti i dati
    if not os.path.exists("data"):
        os.mkdir("data")
        log(tipo="info", args="Creo la cartella data")

    # se non esiste il file per le impostazioni lo creo
    if not os.path.exists("./data/settings.json"):
        setup_settings_file()

    # se il database non esiste lo creo
    if not os.path.exists("./data/database.db"):
        setup_database_file()

    # se non esiste la cartella dei backup la creo
    if not os.path.exists("./data/backup"):
        setup_backup_folder()


# creo il file per le impostazioni
def setup_settings_file():
    with open("./data/settings.json", "w") as data:
        data.write('{\n  "button_width": 33,\n  "button_height": 6,\n  "font": 34,\n  "pad": 12\n}')

    log(tipo="info", args="Creato il file ./data/settings.json")


# creo il file del database
def setup_database_file():
    with open("./data/database.db", "w"):
        # creo la tabella se non esiste
        query_create_table = "CREATE TABLE inventario(codice_prodotto TEXT PRIMARY KEY, nome_prodotto TEXT NOT NULL, " \
                             "quantita INT NOT NULL, prezzo_costo REAL NOT NULL, prezzo REAL NOT NULL);"
        run_query(query_create_table)
        log(tipo="info", args="Creata la tabella inventario")


# creo la cartella per il backup
def setup_backup_folder():
    os.mkdir("./data/backup")
    log(tipo='info', args='creata la cartella di backup')
