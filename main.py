import utils.backend
import utils.frontend
import utils.utils
import utils.setup
import utils.backup


def main():

    # faccio il setup di tutte le cose che mi serviranno
    utils.setup.init_setup()

    # creo il backup
    utils.backup.init_backup(max_num_backup=50)

    # lancio la gui
    utils.frontend.main_menu()



if __name__ == '__main__':
    main()
